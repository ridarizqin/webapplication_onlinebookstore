/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controller;

import Bean.NewCustomerBean;
import Dao.LoginDao;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author lenovo-pc
 */
@WebServlet(name = "LoginServlet", urlPatterns = {"/login"})
public class LoginServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("in post new");
		//PrintWriter out=response.getWriter();
			String userName = request.getParameter("email_id");
			String password = request.getParameter("login_password");
			System.out.println("in servlet username is " + userName +" and password" + password);
			
			NewCustomerBean userBean = new NewCustomerBean();
			userBean.setEmailid(userName);
			userBean.setPassword(password);
			
			String customerId= "" ;
			String customerName= "" ;
			String CustomeremilId= "";
			String customerPhone= "";
                        String CustomerPassword="";
                        
			LoginDao userDao = new LoginDao();
			String userValidate = userDao.user(userBean); //Calling authenticateUser function
			 System.out.println("****userValidate"+userValidate);
			if(userValidate.equals("SUCCESS")) //If function returns success string then user will be rooted to Home page
			 {
				try {
					//System.out.println("%%%123%%%%"+userDao.resultset.getString("CUSTOMER_ID"));
					 customerId = userDao.resultset.getString("CUSTOMER_ID");
					 //System.out.println("****customerId"+customerId);
					 customerName = userDao.resultset.getString("USERNAME").toString();
					 //System.out.println("****customerName"+customerName);
					 CustomeremilId = userDao.resultset.getString("EMAIL_ID");
					 //System.out.println("****CustomeremilId"+CustomeremilId);
					 customerPhone = userDao.resultset.getString("PHONENUMBER");
                                         CustomerPassword = userDao.resultset.getString("PASSWORD");
                                         
					 
					 
					//	System.out.println("****customerPhone"+customerPhone);
						
						
				} 
				catch (SQLException e) 
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
					System.out.println("&&&EROR&&&"+e.getMessage());
				}
				HttpSession session = request.getSession(); //Creating a session
				session.setAttribute("uname", userName); //setting session attribute	
				session.setAttribute("password", password);
				
				/*System.out.println("2222****customerName"+customerName);
				System.out.println("2222****customerPhone"+customerPhone);
				System.out.println("2222****CustomeremilId"+CustomeremilId);
				System.out.println("2222****customerId"+customerId);*/
				
				request.setAttribute("namefromServlet",customerName);
				request.setAttribute("phonefromServlet",customerPhone);
				request.setAttribute("emailfromServlet",CustomeremilId);
				request.setAttribute("customerId",customerId);
                                request.setAttribute("password",CustomerPassword);
                                System.out.println("customerNamen if condition"+customerName);
                                if(customerName.equals("rahmathulla"))
                                {
                                  System.out.println("admin if condition");
                                    request.getRequestDispatcher("/AdminHomePage.jsp").forward(request, response);
                                }
                                else
                                {
                                      System.out.println("user else condition");
                                    request.getRequestDispatcher("/UserHome.jsp").forward(request, response);
                                }
                                
				//request.getRequestDispatcher("/UserHome.jsp").forward(request, response);
				
				
				//request.getRequestDispatcher("/add_book.jsp").forward(request, response);//RequestDispatcher is used to send the control to the invoked page.
			 }
			 else
			 {
				// System.out.println("in servlet else");
				 request.setAttribute("Admin_Login_servlet", false);
							
			 request.setAttribute("errMessage", userValidate); //If authenticateUser() function returns other than SUCCESS string it will be sent to Login page again. Here the error message returned from function has been stored in a errMessage key.
			 request.getRequestDispatcher("/index.html").forward(request, response);//forwarding the request
			 }
			
		
		}
	}


