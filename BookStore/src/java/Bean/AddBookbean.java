/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Bean;

import java.io.InputStream;

/**
 *
 * @author lenovo-pc
 */
public class AddBookbean {
    private String bookName;
   private String authorName;
    private String categoryId;
    private String bookdescription;
    private String totalBooks;
    private String booksSold;
    private String availableBooks;
    private String bookprice;
    private InputStream  Image;
    public InputStream getBytes() 
	{
		return Image;
	}

	public void setBytes(InputStream  bookImage) 
	{
		this.Image = bookImage;
	}

        public String getbookName() {
		return bookName;
	}
	public void setbookName(String bookName) {
		this.bookName = bookName;	
	}
	
	public String getauthorName() {
		return authorName;
	}
	public void setauthorName(String authorName) {
		this.authorName = authorName;	
	}
	
	public String getcategoryId() {
		return categoryId;
	}
	public void setcategoryId(String categoryId) {
		this.categoryId = categoryId;	
	}
	
	public String getbookdescription() {
		return bookdescription;
	}
	public void setbookdescription(String bookdescription) {
		this.bookdescription = bookdescription;	
	}
	public String gettotalBooks() {
		return totalBooks;
	}
	public void settotalBooks(String totalBooks) {
		this.totalBooks = totalBooks;	
	}
	public String getbooksSold() {
		return booksSold;
	}
	public void setbooksSold(String booksSold) {
		this.booksSold = booksSold;	
	}
	public String getavailableBooks() {
		return availableBooks;
	}
	public void setavailableBooks(String availableBooks) {
		this.availableBooks = availableBooks;	
	}
        
	public String getBookPrice() {
		return bookprice;
	}
	public void setBookPrice(String price) {
		this.bookprice = price;	
	}
    
}
