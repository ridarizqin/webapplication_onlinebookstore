package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.util.Base64;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.DriverManager;
import java.sql.Connection;

public final class BookStore_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>JSP Page</title>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <div id=\"navcolor\"><h1 align=\"center\" style=\"color:white\">About Us</h1></div>\n");
      out.write("        ");

                Class.forName("oracle.jdbc.driver.OracleDriver");
                String id = request.getParameter("categoryId");
                Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "system", "123");
                System.out.println("result"+conn);
                String categoryid  = null;
                        
                      categoryid  = request.getParameter("categoryId");
                String qry = null;
                if(categoryid != null)
                {
                    int catId = Integer.parseInt(id);
                    qry = "select BOOK_ID,BOOK_NAME,AUTHOR_NAME,CATEGORY_ID,image,no_of_books_sold,BOOK_PRICE,TOTAL_NO_OF_BOOKS,AVAILABLE_BOOKS,BOOKDESCRIPTION from books121 where CATEGORY_ID="+catId;
                }
                else
                {
                  qry = "select BOOK_ID,BOOK_NAME,AUTHOR_NAME,CATEGORY_ID,image,no_of_books_sold,BOOK_PRICE,TOTAL_NO_OF_BOOKS,AVAILABLE_BOOKS,BOOKDESCRIPTION from books121";  
                }
                Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery(qry);
               System.out.println("result"+rs);

      out.write("\n");
      out.write("\n");
      out.write("        <div id=\"mainpageId\">                                                   \n");
      out.write("<div id=\"displayimage\">\n");
      out.write("    <table id=\"booksdetailsTableId\" class=\"table table-bordered\" style=\"width:66.3%;margin-left: 138px;\">\n");
      out.write("      <tbody>\n");
      out.write("         ");
 while(rs.next()){  
      out.write("\n");
      out.write("        ");
 byte[] imgData = rs.getBytes("IMAGE"); 
        System.out.println("bytes image: "+imgData);
        if(imgData != null)
        {
            String encode = Base64.getEncoder().encodeToString(imgData);
            request.setAttribute("imgBase", encode);
        }
         String iconId = "Icon"+rs.getString("BOOK_ID");
          String BtnId = "btn"+rs.getString("BOOK_ID");
           String bookId = rs.getString("BOOK_ID");
           String b_name = rs.getString("BOOK_NAME");
         System.out.println("***iconId"+iconId);
        
      out.write("\n");
      out.write("        <tr>\n");
      out.write("            <td>\n");
      out.write("                <img src=\"data:image/jpeg;base64,");
      out.write((java.lang.String) org.apache.jasper.runtime.PageContextImpl.evaluateExpression("${imgBase}", java.lang.String.class, (PageContext)_jspx_page_context, null));
      out.write("\" alt=\"\" width=\"210\"\n");
      out.write("\t\t\theight=\"200\" class=\"image\"/></td>\n");
      out.write("            <td>");
      out.print(rs.getString("BOOK_NAME"));
      out.write("</br>");
      out.print(rs.getString("AUTHOR_NAME"));
      out.write("</br><b>");
      out.print(rs.getString("BOOK_PRICE"));
      out.write("Rs</b></br>");
      out.print(rs.getString("BOOKDESCRIPTION"));
      out.write("</td>\n");
      out.write("            <td ><a  href=\"#\" class=\"btn btn-primary a-btn-slide-text mybtn\" onclick=\"modal();\">\n");
      out.write("                    <span class=\"glyphicon glyphicon-edit\" aria-hidden=\"true\"></span>\n");
      out.write("                    <span><strong>Add Cart</strong></span>          \n");
      out.write("                </a> <i class=\"fa fa-check-circle text-success\"  style=\"display:none;\" id=");
      out.print(iconId);
      out.write("></i> </td>\n");
      out.write("            \n");
      out.write("        </tr>\n");
      out.write("                \n");
      out.write("          ");
}
      out.write("  \n");
      out.write("      </tbody> \n");
      out.write("    </table>\n");
      out.write("      <div class=\"modal fade\" id=\"myModal\" role=\"dialog\" data-backdrop=\"static\">\n");
      out.write("    <div class=\"modal-dialog\">\n");
      out.write("    \n");
      out.write("      <!-- Modal content-->\n");
      out.write("      <div class=\"modal-content\">\n");
      out.write("        <div class=\"modal-header\">\n");
      out.write("          <!--button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button-->\n");
      out.write("          <h4 class=\"modal-title\">Order Confirmation</h4>\n");
      out.write("        </div>\n");
      out.write("        <div class=\"modal-body\">\n");
      out.write("          <p>Your order has been placed successfully.</p>\n");
      out.write("        </div>\n");
      out.write("        <div class=\"modal-footer\">\n");
      out.write("            <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\" onclick=\"closeallFunc();\">Close</button>\n");
      out.write("        </div>\n");
      out.write("      </div>\n");
      out.write("      \n");
      out.write("  \n");
      out.write("      <style>\n");
      out.write("          #navcolor\n");
      out.write("            {\n");
      out.write("                background-color:#bc3232 !important;\n");
      out.write("                    height: 50px;\n");
      out.write("            }\n");
      out.write("            \n");
      out.write("          </style>\n");
      out.write("          <script>\n");
      out.write("              function modal()\n");
      out.write("              {\n");
      out.write("                  document.getElementById(\"myModal\").style.display=\"block\";\n");
      out.write("              }\n");
      out.write("          </script>\n");
      out.write("\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
