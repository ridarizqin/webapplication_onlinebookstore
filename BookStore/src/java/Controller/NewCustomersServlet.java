/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controller;

import Bean.NewCustomerBean;
import Dao.NewCustomerDao;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author lenovo-pc
 */
@WebServlet(name = "NewCustomersServlet", urlPatterns = {"/newcustomer"})
public class NewCustomersServlet extends HttpServlet {

   private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */




	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//doGet(request, response);
		
		String enteredname = request.getParameter("name");
		String enteredphone = request.getParameter("phone");
		String enteredemail = request.getParameter("email");
		String enteredpassword = request.getParameter("password");
		String enteredconfirmPassword = request.getParameter("confirmPassword");
		
		System.out.println("%%%%% name: "+enteredname);
		System.out.println("%%%%% enteredphone: "+enteredphone);
		System.out.println("%%%%% enteredemail: "+enteredemail);
		System.out.println("%%%%% enteredpassword: "+enteredpassword);
		System.out.println("%%%%% enteredconfirmPassword: "+enteredconfirmPassword);
		NewCustomerBean instance = new NewCustomerBean();
		instance.setUsername(enteredname);
		instance.setPhoneNumber(enteredphone);
		instance.setPassword(enteredpassword);
		instance.setEmailid(enteredemail);
		instance.setConfirmPassword(enteredconfirmPassword);
		
		NewCustomerDao daoInstance = new NewCustomerDao();
		String userRegistered = daoInstance.authenticateUser(instance);
		if(userRegistered.equals("SUCCESS"))
		{
			HttpSession session = request.getSession();
			session.setAttribute("userName",enteredname);
			request.setAttribute("namefromServlet",enteredname);
			request.setAttribute("phonefromServlet",enteredphone);
			request.setAttribute("emailfromServlet",enteredemail);
			request.getRequestDispatcher("/UserHome.jsp").forward(request, response);
		}
		else
		{
			request.getRequestDispatcher("/index.html").forward(request, response);
		}
		
	}

}
