package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class AddCategory_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("<head>\n");
      out.write("<meta charset=\"utf-8\">\n");
      out.write("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">\n");
      out.write("<link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css\">\n");
      out.write("<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css\">\n");
      out.write("<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>\n");
      out.write("<script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js\"></script>\n");
      out.write("<script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js\"></script>\n");
      out.write("<link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.2.0/css/all.css\">\n");
      out.write(" <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css\">\n");
      out.write(" <style>\n");
      out.write("      #maincontainer\n");
      out.write(" {\n");
      out.write("\t    border-style: groove ;\n");
      out.write(" \n");
      out.write(" }\n");
      out.write(" .homeBtnCls\n");
      out.write("{\n");
      out.write("\tbackground-color: #bc3232 !important;\n");
      out.write("\tborder:none;\n");
      out.write("}\n");
      out.write(".main-cls\n");
      out.write("{\n");
      out.write("   margin: 20px;\n");
      out.write("}\n");
      out.write("\t\n");
      out.write(".bg-light\n");
      out.write("{\n");
      out.write("\t\tbackground-color: #bc3232 !important;\n");
      out.write("}\n");
      out.write(".fontcls\n");
      out.write("{\n");
      out.write("\tcolor:white !important;\n");
      out.write("}\n");
      out.write(".fa-home\n");
      out.write("{\n");
      out.write("\tcolor:white;\n");
      out.write("}\n");
      out.write(".nav > li > a:hover \n");
      out.write("{\n");
      out.write("\n");
      out.write("\tbackground-color:black !important;\n");
      out.write("}\n");
      out.write(".btn-group-lg>.btn, .btn-lg \n");
      out.write("{\n");
      out.write("    line-height: normal !important;\n");
      out.write("}\n");
      out.write(".btn-info \n");
      out.write("{\n");
      out.write("    background-color: #404040 !important;\n");
      out.write("}\n");
      out.write("\n");
      out.write("\n");
      out.write(" #maincontainer\n");
      out.write(" {\n");
      out.write("\t    border-style: groove ;\n");
      out.write(" \n");
      out.write(" }\n");
      out.write(" #cancel\n");
      out.write(" {\n");
      out.write("     margin-top: 22px;\n");
      out.write("    margin-bottom: 18px;\n");
      out.write("    margin_left: 51px;\n");
      out.write("    margin-left: -309px;\n");
      out.write("    width: 73px;\n");
      out.write("    padding: 4px 5px;\n");
      out.write(" // margin: 8px 0;\n");
      out.write("  display: inline-block;\n");
      out.write("  border: 1px solid #ccc;\n");
      out.write("  border-radius: 4px;\n");
      out.write("  box-sizing: border-box;\n");
      out.write("  //background-color:slategrey;\n");
      out.write("  background: #404040;\n");
      out.write("      color: #fff !important;\n");
      out.write("\t  //width:16%;\n");
      out.write("\t  //margin-left: 169px;\n");
      out.write("\n");
      out.write(" }\n");
      out.write(" #Add\n");
      out.write(" {\n");
      out.write(" padding: 4px 5px;\n");
      out.write(" // margin: 8px 0;\n");
      out.write("  display: inline-block;\n");
      out.write("  border: 1px solid #ccc;\n");
      out.write("  border-radius: 4px;\n");
      out.write("  box-sizing: border-box;\n");
      out.write("  //background-color:slategrey;\n");
      out.write("  background: #bc3232;\n");
      out.write("      color: #fff !important;\n");
      out.write("\t  width:16%;\n");
      out.write("\t  margin-left: 169px;\n");
      out.write("}\n");
      out.write("</style>\n");
      out.write("<body>\n");
      out.write("    <div class=\"main-cls\">\n");
      out.write("    <nav class=\"navbar navbar-expand-md navbar-light bg-light\">\n");
      out.write("\t<a href=\"#\" class=\"navbar-brand fontcls\">BookStore</a>\n");
      out.write("\t\t <!--a class=\"navbar-brand\" href=\"#\"><i class=\"fas fa-home\"></i></a-->\n");
      out.write("\t\t <!--button class=\"navbar-brand homeBtnCls\" onClick=\"document.location.reload(true)\"><span class=\"fas fa-home\"></span></button>\n");
      out.write("\t\t <!--button type=\"button\" class=\"navbar-toggler\" data-toggle=\"collapse\" data-target=\"#navbarCollapse\">\n");
      out.write("            <span class=\"navbar-toggler-icon\"></span>\n");
      out.write("        </button-->\n");
      out.write("        <!--div id=\"navbarCollapse\" class=\"collapse navbar-collapse\">\n");
      out.write("            <ul class=\"nav navbar-nav\">\n");
      out.write("\t\t\t   <!--li class=\"nav-item\">\n");
      out.write("                    <a href=\"#\" class=\"nav-link fontcls\" >Home</a>\n");
      out.write("                </li-->\n");
      out.write("               \n");
      out.write("                <!--li class=\"nav-item dropdown\">\n");
      out.write("                    <a href=\"#\" class=\"nav-link dropdown-toggle fontcls\" data-toggle=\"dropdown\">Books</a>\n");
      out.write("                    <div class=\"dropdown-menu\">\n");
      out.write("                        <a href=\"AddBook.jsp\" class=\"dropdown-item\">Add</a>\n");
      out.write("                        <a href=\"#\" onclick=\"showTable();\" class=\"dropdown-item\">delete</a>\n");
      out.write("                        <a href=\"#\" class=\"dropdown-item\">update</a>\n");
      out.write("                        \n");
      out.write("\t\t\t\t\t\t<!--div class=\"dropdown-divider\"></div-->\n");
      out.write("                        \n");
      out.write("                    <!--/div>\n");
      out.write("                </li>\n");
      out.write("\t\t\t\t<li class=\"nav-item dropdown\">\n");
      out.write("                    <a href=\"#\" class=\"nav-link dropdown-toggle fontcls\" data-toggle=\"dropdown\">Category</a>\n");
      out.write("                    <div class=\"dropdown-menu\">\n");
      out.write("                        <a href=\"#\" class=\"dropdown-item\">Add</a>\n");
      out.write("                        <a href=\"#\" class=\"dropdown-item\" >delete</a>\n");
      out.write("                        <a href=\"#\" class=\"dropdown-item\">update</a>\n");
      out.write("                        \n");
      out.write("\t\t\t\t\t\t<!--div class=\"dropdown-divider\"></div-->\n");
      out.write("                        \n");
      out.write("                    <!--/div>\n");
      out.write("                </li>\n");
      out.write("            </ul>\n");
      out.write("            <ul class=\"nav navbar-nav\">\n");
      out.write("\t\t\t\t\t   <li class=\"nav-item dropdown\">\n");
      out.write("\t\t\t\t\t\t\t<a href=\"#\" class=\"nav-link dropdown-toggle fontcls\" data-toggle=\"dropdown\">Reports</a>\n");
      out.write("\t\t\t\t\t\t\t<div class=\"dropdown-menu\">\n");
      out.write("\t\t\t\t\t\t\t\t<a href=\"#\" class=\"dropdown-item\">My Account</a>\n");
      out.write("\t\t\t\t\t\t\t\t<a href=\"#\" class=\"dropdown-item\">My Orders</a>\n");
      out.write("\t\t\t\t\t\t\t\t<a href=\"#\"class=\"dropdown-item\">Logout</a>\n");
      out.write("\t\t\t\t\t\t\t</div>\n");
      out.write("\t\t\t\t\t\t</li>\n");
      out.write("\t\t\t\t\t</ul>\n");
      out.write("            <!--button class=\"btn btn-info btn-lg\"  type=\"submit\" onclick=\"Loginfunc();\" style=\"margin-left: 608px;\"> <span class=\"glyphicon glyphicon-log-in\"></span> Log in</button-->\n");
      out.write("\t\t\t\t\n");
      out.write("        <!--/div-->\n");
      out.write("    </nav>\n");
      out.write("</div>\n");
      out.write("\n");
      out.write("<div class=\"container\" id=\"maincontainer\">\n");
      out.write("    <form  action=\"AddCategory\" method=\"post\" >\n");
      out.write("    <div class=\"row\">\n");
      out.write("\t\t<div class=\"col-10\">\n");
      out.write("\t\t\t<label for=\"categoryid\" style=\"width: 130px;\">Category Id</label>\n");
      out.write("\t\t\t<input type=\"text\" id=\"Bname\" name=\"categoryid\" style=\"width:200px;margin-top: 28px;\" placeholder=\"Category Id\">\n");
      out.write("\t\t\n");
      out.write("      </div>\n");
      out.write("\t  <div class=\"col-2\">\n");
      out.write("\t  </div>\n");
      out.write("\t</div>\n");
      out.write("        <div class=\"row\">\n");
      out.write("      \n");
      out.write("        \n");
      out.write("      \n");
      out.write("      <div class=\"col-10\">\n");
      out.write("\t  <label for=\"Categoryname\" style=\"width:130px;\">Category Name</label>\n");
      out.write("        <input type=\"text\" id=\"Aname\" name=\"Categoryname\" style=\"width:200px;\" placeholder=\"CategoryName\">\n");
      out.write("      </div>\n");
      out.write("\t  <div class=\"col-2\">\n");
      out.write("    </div>\n");
      out.write("\t</div>\n");
      out.write("\t    \t<div class=\"row\">\n");
      out.write("\t\t\n");
      out.write("\t\n");
      out.write("\t<div class=\"col-6\">\n");
      out.write("\t\t<input type=\"submit\"  value=\"Add\" id=\"Add\" style=\"margin-top: 20px;margin-bottom: 18px;\" >\n");
      out.write("\t </div>\n");
      out.write("\t <div class=\"col-3\">\n");
      out.write("             <input type=\"button\"  value=\"Cancel\" id=\"cancel\" style=\"margin-top: 20px;margin-bottom: 18px;\" onclick=\"openPage('AdminHomePage.jsp')\" />\n");
      out.write("\t </div>\n");
      out.write("\t </div>\n");
      out.write("    </form> \n");
      out.write("  \n");
      out.write("</div>\n");
      out.write("    <script>\n");
      out.write("        function openPage(pageURL)\n");
      out.write("        {\n");
      out.write("            window.location.href= pageURL;\n");
      out.write("        }\n");
      out.write("    </script>\n");
      out.write("\n");
      out.write("</body>\n");
      out.write("</head>\n");
      out.write("\n");
      out.write("</html>");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
