
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css">
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
 <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
 <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"/>
 <style>
  .mybtn
  {
      background-color: #bc3232 !important;
  }
  .mydelbtn
  {
      background-color: #404040 !important;
  }
 #maincontainer
 {
	    border-style: groove ;
 
 }
 .homeBtnCls
{
	background-color: #bc3232 !important;
	border:none;
}
.main-cls
{
   margin: 20px;
}
	
.bg-light
{
		background-color: #bc3232 !important;
}
.fontcls
{
	color:white !important;
}
.fa-home
{
	color:white;
}
.nav > li > a:hover 
{

	background-color:black !important;
}
.btn-group-lg>.btn, .btn-lg 
{
    line-height: normal !important;
}
.btn-info 
{
    background-color: #404040 !important;
}

 #Add
 {
 padding: 4px 5px;
 // margin: 8px 0;
  display: inline-block;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-sizing: border-box;
  //background-color:slategrey;
  background: #bc3232;
      color: #fff !important;
	  width:16%;
	  margin-left: 169px;
}
a.btn:hover {
     -webkit-transform: scale(1.1);
     -moz-transform: scale(1.1);
     -o-transform: scale(1.1);
 }
 a.btn {
     -webkit-transform: scale(0.8);
     -moz-transform: scale(0.8);
     -o-transform: scale(0.8);
     -webkit-transition-duration: 0.5s;
     -moz-transition-duration: 0.5s;
     -o-transition-duration: 0.5s;
 }
 #booksdetailsTableId_paginate
 {
     
     margin-right: 270px !important;
 }
</style>
<body>
   <%@page import="java.util.*"%>
	<%@page import="java.sql.*"%>
	<%
		String id = request.getParameter("id");
                System.out.println("Admin Home Id: "+id);
		Class.forName("oracle.jdbc.driver.OracleDriver");
		Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "system", "123");
		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery("select BOOK_ID,BOOK_NAME,AUTHOR_NAME,CATEGORY_ID,BOOK_PRICE,TOTAL_NO_OF_BOOKS,AVAILABLE_BOOKS,BOOKDESCRIPTION from books121");
                Statement st_category = conn.createStatement();
		ResultSet rs_category = st_category.executeQuery("select CATEGORY_ID,category_name from categories");
       System.out.println("1111"+rs_category);
                %>  
<div class="main-cls">
    <nav class="navbar navbar-expand-md navbar-light bg-light">
	<a href="#" class="navbar-brand fontcls">BookStore</a>
		 <!--a class="navbar-brand" href="#"><i class="fas fa-home"></i></a-->
		 <button class="navbar-brand homeBtnCls" onclick="openPage('index.html')"><span class="fas fa-home"></span></button>
		 <!--button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
            <span class="navbar-toggler-icon"></span>
        </button-->
        <div id="navbarCollapse" class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
			   <!--li class="nav-item">
                    <a href="#" class="nav-link fontcls" >Home</a>
                </li-->
               
                <li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle fontcls" data-toggle="dropdown">Books</a>
                    <div class="dropdown-menu">
                        <a href="AddBook.jsp" class="dropdown-item">Add</a>
                        <a href="#" onclick="showTable();" class="dropdown-item">delete/update/view</a>
                        <!--a href="#" class="dropdown-item">update</a-->
                        
						<!--div class="dropdown-divider"></div-->
                        
                    </div>
                </li>
				<li class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle fontcls" data-toggle="dropdown">Category</a>
                    <div class="dropdown-menu">
                        <a href="AddCategory.jsp" class="dropdown-item">Add</a>
                        <a href="#" onclick="showCategoryTable();" class="dropdown-item" >delete/update/view</a>
                        <!--a href="#" class="dropdown-item">update</a-->
                        
						<!--div class="dropdown-divider"></div-->
                        
                    </div>
                </li>
            </ul>
            <ul class="nav navbar-nav">
					   <li class="nav-item dropdown">
							<a href="#" class="nav-link dropdown-toggle fontcls" data-toggle="dropdown">Reports</a>
							<div class="dropdown-menu">
								<a href="#" class="dropdown-item">By Date</a>
								<a href="#" class="dropdown-item">By Month</a>
								<!--a href="#"class="dropdown-item" onclick="openPage('index.html')">Logout</a-->
							</div>
						</li>
					</ul>
            <!--button class="btn btn-info btn-lg"  type="submit" onclick="Loginfunc();" style="margin-left: 608px;"> <span class="glyphicon glyphicon-log-in"></span> Log in</button-->
				
        </div>
    </nav>
</div>
<!-- books table -->
<div id="displayimageid">

<table id="booksdetailsTableId" class="table table-bordered" style="width:66.3%;margin-left: 138px;display:none;">
    <thead>
        <tr>
            <th>Action</th>
            <th>BookId</th>
            <th>Book Name</th>
            <th>Author Name</th>
            <th>Price</th>
            <th>Total Books</th>
            <th>Available Books</th>
        </tr>
    </thead>
    <tbody>
         <% while(rs.next()){ %>
        <tr>
            <td>
                <a href="BookEdit.jsp?id=<%=rs.getString("BOOK_ID")%>" class="btn btn-primary a-btn-slide-text mybtn">
                    <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                    <span><strong>Edit</strong></span>            
                </a>
                <a href="DeleteBook.jsp?id=<%=rs.getString("BOOK_ID")%>" class="btn btn-primary a-btn-slide-text mydelbtn">
                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                    <span><strong>Delete</strong></span>            
                </a>
            </td>
            <td><%=rs.getString("BOOK_ID")%></td>
            <td><%=rs.getString("BOOK_NAME")%></td>
            <td><%=rs.getString("AUTHOR_NAME")%></td>
            <td><%=rs.getInt("BOOK_PRICE")%></td>
            <td><%=rs.getInt("TOTAL_NO_OF_BOOKS")%></td>
            <td><%=rs.getInt("AVAILABLE_BOOKS")%></td>
        </tr>
        <% } %>
    </tbody>

</table>
    <div>
        <table id="categorydetailsTableId" class="table table-bordered" style="width:66.3%;margin-left: 138px;display:none;">
            <thead>
        <tr>
            <th>Action</th>
            <th>CategoryId</th>
            <th>CategoryName</th>
            </tr>
    </thead>
    <tbody>
         <% while(rs_category.next()){ %>
        <tr>
            
            <td>
            <a href="#" class="btn btn-primary a-btn-slide-text mybtn">
                    <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                    <span><strong>Edit</strong></span>            
                </a>
                <a href="#" class="btn btn-primary a-btn-slide-text mydelbtn">
                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                    <span><strong>Delete</strong></span>            
                </a>
            </td>
            <td><%=rs_category.getString("CATEGORY_ID")%></td>
            <td><%=rs_category.getString("CATEGORY_NAME")%></td>
            
        </tr>
        <% } %>
    </tbody>

        
            
        </table>
    </div>
    
</div>

<div id="imagesId" class="container imgmaincls">
<div class="row">
   <div class="col-1"></div>
        <div class="col-12">
            <img class="mySlides" src="images/Libraries.jpg" style="width:100%">
            <img class="mySlides" src="images/img1.jpg" style="width:100%">
            <img class="mySlides" src="images/3.png" style="width:100%">
            <img class="mySlides" src="images/img2.jpg" style="width:100%">
        </div> 
    <div class="col-1"></div>
</div>
<!-- Modal -->
  <!--div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <!--div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Delete Confirmation</h4>
        </div>
        <div class="modal-body">
          <p>Data has been deleted successfully.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div-->
<!--div class="w3-content w3-section" style="max-width:500px">
  
</div-->
</div>
<script type="text/javascript">
    
    $(document).ready( function () {
      //  $('#myModal').modal('hide');
    if(<%=id %> != null)
    {
        //alert('coming dude');
        document.getElementById("imagesId").style.display = "none";
         document.getElementById("booksdetailsTableId").style.display = "block";
        // $('#myModal').modal('show');
    }
   
} );
    var myIndex = 0;
    carousel();

    function carousel() {
      var i;
      var x = document.getElementsByClassName("mySlides");
      for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";  
      }
      myIndex++;
      if (myIndex > x.length) {myIndex = 1}    
      x[myIndex-1].style.display = "block";  
      setTimeout(carousel, 2000); // Change image every 2 seconds
    }
function showTable()
{
    debugger;
    document.getElementById("imagesId").style.display = "none";
    $('#booksdetailsTableId').DataTable(
            {
                "searching": false,
                info: false,
                "lengthChange": false
            });
    document.getElementById("booksdetailsTableId").style.display = "block";
    document.getElementById("categorydetailsTableId").style.display = "none";
}
function showCategoryTable()
{
    debugger;
    document.getElementById("imagesId").style.display = "none";
   /* $('#categorydetailsTableId').DataTable(
            {
                "searching": false,
                info: false,
                "lengthChange": false
            });*/
    document.getElementById("categorydetailsTableId").style.display = "block";
    document.getElementById("booksdetailsTableId").style.display = "none";
}

   function openPage(pageURL)
        {
           // alert('this is working');
            window.location.href=pageURL;
        }

</script>


