package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.DriverManager;
import java.sql.Connection;

public final class myOrders_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("                \n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <meta charset=\"utf-8\">\n");
      out.write("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">\n");
      out.write("<link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css\">\n");
      out.write("<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css\">\n");
      out.write("<script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>\n");
      out.write("<script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js\"></script>\n");
      out.write("<script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js\"></script>\n");
      out.write("<link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.2.0/css/all.css\">\n");
      out.write(" <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css\">\n");
      out.write("\n");
      out.write("        <title>JSP Page</title>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("        <script>\n");
      out.write("                    .ordercolor\n");
      out.write("            {\n");
      out.write("                background-color:#404040 !important;\n");
      out.write("            }\n");
      out.write("        </script>\n");
      out.write("        ");

		String custid = request.getParameter("customerId");
                int id = Integer.parseInt(custid);
                System.out.println("Customer  Id: "+custid);
                Class.forName("oracle.jdbc.driver.OracleDriver");
		Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "system", "123");
		Statement st = conn.createStatement();
                String qry = "select ORDER_ID,CUSTOMER_ID,ORDER_STATUS,ORDER_PLACED_DATE,TOTAL_AMOUNT from orders where CUSTOMER_ID="+id;
                System.out.println("***qry: "+qry);
                ResultSet rs = st.executeQuery(qry);
                System.out.println("***rs  Id: "+rs);
                
      out.write("\n");
      out.write("                <div class=\"main-cls\">\n");
      out.write("    <nav class=\"navbar navbar-expand-md navbar-light bg-light\">\n");
      out.write("\t<a href=\"#\" class=\"navbar-brand fontcls\" align=\"center\">BookStore</a>\n");
      out.write("                </div>\n");
      out.write("                \n");
      out.write("        <div class=\"container\">\n");
      out.write("<table id=\"cartTableID\" class=\"table table-bordered\" style=\"width:66.3%;margin-left: 138px;margin-top: 13px;\">\n");
      out.write("\t<thead>\n");
      out.write("      <tr>\n");
      out.write("\t  <th></th>\n");
      out.write("\t  <th>Order Id</th>\n");
      out.write("        <th>Ordered Date</th>\n");
      out.write("        <th>Total Amount</th>\n");
      out.write("\t\t</tr>\n");
      out.write("    </thead>\n");
      out.write("\t<tbody>\n");
      out.write("\t ");
 while(rs.next()){  
      out.write("\n");
      out.write("\t<tr>\n");
      out.write("      <td>\n");
      out.write("\t\t<!--button type=\"button\" > Order Details</button-->\n");
      out.write("                <a  href=\"OrderItems.jsp?orderId=");
      out.print(rs.getInt("ORDER_ID"));
      out.write("\" class=\"btn btn-primary a-btn-slide-text mybtn ordercolor\" >Order Details</a>\n");
      out.write("\t  </td>\n");
      out.write("      <td>\n");
      out.write("\t\t<input type=\"text\"  name=\"orderId\" value=\"");
      out.print(rs.getInt("ORDER_ID"));
      out.write("\"/>\n");
      out.write("\t  </td>\n");
      out.write("      <td>\n");
      out.write("\t\t<input type=\"text\"  name=\"date\" value=\"");
      out.print(rs.getDate("ORDER_PLACED_DATE"));
      out.write("\"/>\n");
      out.write("\t  </td>;\n");
      out.write("      <td>\n");
      out.write("\t\t <input type=\"text\"  name=\"Amount\" value=\"");
      out.print(rs.getFloat("TOTAL_AMOUNT"));
      out.write("\"/>\n");
      out.write("\t  </td>;\n");
      out.write("       </tr>;\n");
      out.write("\t    ");
}
      out.write("  \n");
      out.write("     </tbody>\n");
      out.write("</table>\n");
      out.write("</div>\n");
      out.write("  \n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
