/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controller;

import DB.DbConnection;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONException;

/**
 *
 * @author lenovo-pc
 */
@WebServlet(name = "EditBookServlet", urlPatterns = {"/EditBook"})
public class EditBookServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        /*response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
           /* out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet EditBookServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet EditBookServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }*/
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    @SuppressWarnings("empty-statement")
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
         System.out.println("save book details method called: ");
        String[] myJsonData = request.getParameterValues("BookDetails");
        System.out.println("myJsonData: "+myJsonData);
        System.out.println("myJsonData first: "+myJsonData[0]);
        JSONArray jArray = null;    
        String BookName = null;
        String BookId = null;
        String AuthorName = null;
        String description = null;
        String price = null;
        String category = null;
        String totalBooks = null;
    try {
             jArray = new JSONArray(myJsonData[0]);
              BookName = (String)jArray.get(1);
         BookId = (String) jArray.get(0);
         AuthorName = (String) jArray.get(2);
         description = (String) jArray.get(4);
         price = (String) jArray.get(5);
         category = (String) jArray.get(3);
         totalBooks = (String) jArray.get(6);
        } 
    catch (JSONException ex) 
        {
            System.out.println("Error in json Array: "+myJsonData);  
        }
    System.out.println("jArray first: "+jArray);
 System.out.println("jArray first: "+jArray.length());
 
 System.out.println("BookId first: "+BookId);
 System.out.println("BookName first: "+BookName);
 System.out.println("AuthorName first: "+AuthorName);
 System.out.println("description first: "+description);
 System.out.println("price first: "+price);
 System.out.println("category first: "+category);
 System.out.println("totalBooks first: "+totalBooks);
 Connection conn = null;  
 try{
                  conn = DbConnection.conn();
                  System.out.println("connection success ");
                   String qry="update books121 set BOOK_NAME=?,AUTHOR_NAME=?,CATEGORY_ID=?,BOOKDESCRIPTION=?,BOOK_PRICE=?,TOTAL_NO_OF_BOOKS=?  WHERE BOOK_ID = ?";
                    PreparedStatement st = conn.prepareStatement(qry);
                    st.setString(1,BookName);
                    st.setString(2,AuthorName);
                    st.setInt(3,Integer.parseInt(category));
                    st.setString(4,description);
                    st.setInt(5,Integer.parseInt(price));
                    st.setInt(6,Integer.parseInt(totalBooks));
                    st.setInt(7,Integer.parseInt(BookId));
                    int i = st.executeUpdate();
   System.out.println("final update: "+i);
 }
    catch(Exception e1)
    {
 System.out.println("SQL ERROR update: "+e1.getMessage());
    }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
