/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Controller;

import Bean.AddBookBean1;
import Dao.AddBookDao1;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

/**
 *
 * @author lenovo-pc
 */
@WebServlet(name = "AddBookServlet1", urlPatterns = {"/AddBook1"})
public class AddBookServlet1 extends HttpServlet {

    /**
     * @see HttpServlet#HttpServlet()
     */
    public AddBookServlet1() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
        response.setContentType("text/html;charset=UTF-8");
        System.out.println("in servlet");
		//PrintWriter out=response.getWriter();
		String bookName = request.getParameter("Bookname").trim();
        String authorName = request.getParameter("authorName").trim();
        String categoryId = request.getParameter("category").trim();
        String bookdescription = request.getParameter("bookDescription").trim();
        String totalBooks = request.getParameter("TotalNoOfBooks").trim();
        String booksSold = request.getParameter("NoOfBooksSold").trim();
        String availableBooks = request.getParameter("AvailableBooks").trim();
        //String price = request.getParameter("bookprice").trim();
        
		Part image=request.getPart("myFile");
		System.out.println("getting parameters");
		AddBookBean1 bookBean = new AddBookBean1();

		
		bookBean.setBookName(bookName);
		bookBean.setAuthorName(authorName);
		bookBean.setBookDescription(bookdescription);
		//bookBean.setPrice(price);
		bookBean.setCategoryId(categoryId);
		bookBean.setImage(image);
		bookBean.setTotalBooks(totalBooks);
                bookBean.setAvailableBooks(availableBooks);
                bookBean.setSoldBooks(booksSold);
                
                System.out.println("Add Book name");
                System.out.println("Add book Author name");
                System.out.println("add book bookdescription"+bookdescription);
                System.out.println("add book categoryId"+categoryId);
                System.out.println("add book totalBooks"+totalBooks);
                System.out.println("add book availableBooks"+availableBooks);
                System.out.println("add book booksSold"+booksSold);
                
                
		

		AddBookDao1 userdao = new AddBookDao1();

		String userRegistered = userdao.book(bookBean);
                System.out.println("in if"+userRegistered);
		/*if (userRegistered.equals("SUCCESS")) // On success, you can display a message to user on Home page
		{
			System.out.println("in if");
			//request.setAttribute("Admin_Menu", true);
			HttpSession session = request.getSession(false); 
			request.getRequestDispatcher("/AddBook.jsp").forward(request, response);
		} else // On Failure, display a meaningful message to the User.
		{
			System.out.println("in else");
			
			request.setAttribute("errMessage", userRegistered);
			request.getRequestDispatcher("/index.html").forward(request, response);
		}*/

	}

}
