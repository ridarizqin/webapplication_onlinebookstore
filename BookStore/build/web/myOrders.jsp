<%-- 
    Document   : myOrders
    Created on : 6 Aug, 2019, 9:29:02 PM
    Author     : lenovo-pc
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

                
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css">
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">

        <title>JSP Page</title>
    </head>
    <body>
        <style>
             #ordercolor
            {
                background-color:#bc3232 !important;
            }
            #navcolor
            {
                background-color:#bc3232 !important;
            }
        </style>
        <%
		String custid = request.getParameter("customerId");
                int id = Integer.parseInt(custid);
                System.out.println("Customer  Id: "+custid);
                Class.forName("oracle.jdbc.driver.OracleDriver");
		Connection conn = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "system", "123");
		Statement st = conn.createStatement();
                String qry = "select ORDER_ID,CUSTOMER_ID,ORDER_STATUS,ORDER_PLACED_DATE,TOTAL_AMOUNT from orders where CUSTOMER_ID="+id;
                System.out.println("***qry: "+qry);
                ResultSet rs = st.executeQuery(qry);
                System.out.println("***rs  Id: "+rs);
                %>
                <div id="navcolor"><h3 align="center" style="color:white">order details</h3></div>
                
        <div class="container">
<table id="cartTableID" class="table table-bordered" style="width:66.3%;margin-left: 138px;margin-top: 13px;">
	<thead>
      <tr>
	  <th></th>
	  <th>Order Id</th>
        <th>Ordered Date</th>
        <th>Total Amount</th>
		</tr>
    </thead>
	<tbody>
	 <% while(rs.next()){  %>
	<tr>
      <td>
		<!--button type="button" > Order Details</button-->
                <a  href="OrderItems.jsp?orderId=<%=rs.getInt("ORDER_ID")%>" class="btn btn-primary a-btn-slide-text mybtn " id="ordercolor" >Order Details</a>
	  </td>
      <td>
		<input type="text"  name="orderId" value="<%=rs.getInt("ORDER_ID")%>"/>
	  </td>
      <td>
		<input type="text"  name="date" value="<%=rs.getDate("ORDER_PLACED_DATE")%>"/>
	  </td>;
      <td>
		 <input type="text"  name="Amount" value="<%=rs.getFloat("TOTAL_AMOUNT")%>"/>
	  </td>;
       </tr>;
	    <%}%>  
     </tbody>
</table>
</div>
  
    </body>
</html>
