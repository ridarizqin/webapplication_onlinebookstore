
<%@page import="java.util.Base64"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ page import="java.sql.*" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Bootstrap 4 Dropdowns within a Navbar</title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

<!--link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"-->
 <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css">
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
 <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script> 
<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<title>Insert title here</title>
</head>
<body>
<%  
String name=request.getParameter("srch-term"); 
System.out.println("***Name in search: "+name);
ResultSet rs=null;
if(name==null||name.trim().equals(""))
{  
//out.print(" ");  
    System.out.println("value not found");
}
else
{  
 
Class.forName("oracle.jdbc.driver.OracleDriver");  
Connection con=DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe","system","123");  
PreparedStatement ps=con.prepareStatement("select * from books121 where book_name like '"+name+"%'");  
 rs=ps.executeQuery();
}
%> 
  
<div id="mainpageId">                                                   
<div id="displayimage">
    <table id="booksdetailsTableId" class="table table-bordered" style="width:66.3%;margin-left: 138px;">
      <tbody>
         <% while(rs.next()){  %>
        <% byte[] imgData = rs.getBytes("IMAGE"); 
        System.out.println("bytes image: "+imgData);
        if(imgData != null)
        {
            String encode = Base64.getEncoder().encodeToString(imgData);
            request.setAttribute("imgBase", encode);
        }
         String iconId = "Icon"+rs.getString("BOOK_ID");
          String BtnId = "btn"+rs.getString("BOOK_ID");
           String bookId = rs.getString("BOOK_ID");
           String b_name = rs.getString("BOOK_NAME");
         System.out.println("***iconId"+iconId);
        %>
        <tr>
            <td>
                <img src="data:image/jpeg;base64,${imgBase}" alt="" width="210"
			height="200" class="image"/></td>
            <td><%=rs.getString("BOOK_NAME")%></br><%=rs.getString("AUTHOR_NAME")%></br><b><%=rs.getString("BOOK_PRICE")%>Rs</b></br><%=rs.getString("BOOKDESCRIPTION")%></td>
            <td ><a  href="#" class="btn btn-primary a-btn-slide-text mybtn" onclick="addtocartFunc(<%=bookId%>,<%=rs.getString("BOOK_PRICE")%>,'<%= b_name %>');" id=<%= BtnId %>>
                    <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                    <span><strong>Add Cart</strong></span>          
                </a> <i class="fa fa-check-circle text-success"  style="display:none;" id=<%=iconId%>></i> </td>
            
        </tr>
                
          <%}%>  
      </tbody> 
    </table>
      <button type="button" class="btn btn-primary"onclick="checkoutFunc();">checkout</button>
</div>
      <style>
          .mybtn
  {
      background-color: #bc3232 !important;
  }
      </style>

</body>
</html>